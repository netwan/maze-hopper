package de.mazehopper;

import javax.media.j3d.*;
import javax.vecmath.*;

import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.SimpleUniverse;
import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;

import de.mazehopper.map.FieldType;
import de.mazehopper.map.Map;

@SuppressWarnings("restriction")

public class Display {
	private SimpleUniverse universe;
	private Map map;
	
	public Display(Map map) {
		this.map = map;
		
		universe = new SimpleUniverse();
		
		universe.getViewingPlatform().setNominalViewingTransform();
		universe.getViewer().getView().setMinimumFrameCycleTime(20);
		universe.addBranchGraph(createSceneGraph());
		
		OrbitBehavior orbit = new OrbitBehavior(universe.getCanvas(), OrbitBehavior.REVERSE_ALL);
		BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0), 100.0);
		orbit.setSchedulingBounds(bounds);
		universe.getViewingPlatform().setViewPlatformBehavior(orbit);
	}
	
	private BranchGroup createSceneGraph() {
		BranchGroup objRoot = new BranchGroup();
		
		TransformGroup objTrans = new TransformGroup();
		//objTrans.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		objRoot.addChild(objTrans);
		
		Transform3D sceneTransform = new Transform3D();
		//transform.rotX(Math.toRadians(-40));
		sceneTransform.setScale(8.0/map.getWidth());
		objTrans.setTransform(sceneTransform);
		
		// add map
		float mx = map.getWidth() / 2.0f;
		float my = map.getHeight() / 2.0f;
		for (int x = 0; x < map.getWidth(); x++) {
			for (int y = 0; y < map.getHeight(); y++) {
				if (map.getFieldAt(x, y) == FieldType.WALL)
					continue;
				
				Transform3D transform = new Transform3D();
				Vector3f pos = new Vector3f((x-mx)*0.15f, (my-y)*0.15f, 0.0f);
				transform.setTranslation(pos);
				
				TransformGroup tg = new TransformGroup(transform);
				Box box = new Box(0.05f, 0.05f, 0.05f, null);
				tg.addChild(box);
				
				objTrans.addChild(tg);
			}
		}
		
		// add light
		Color3f light1Color = new Color3f(.1f, 1.4f, .1f); // green light
		BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0), 100.0);
		Vector3f light1Direction = new Vector3f(4.0f, -7.0f, -12.0f);
		DirectionalLight light1 = new DirectionalLight(light1Color, light1Direction);
		light1.setInfluencingBounds(bounds);
		objTrans.addChild(light1);
		
		// add light from below
		Color3f light2Color = new Color3f(1.4f, .1f, .1f); // red light
		Vector3f light2Direction = new Vector3f(-4.0f, 7.0f, 12.0f);
		DirectionalLight light2 = new DirectionalLight(light2Color, light2Direction);
		light2.setInfluencingBounds(bounds);
		objTrans.addChild(light2);
		
		objRoot.compile();
		return objRoot;
	}
}
