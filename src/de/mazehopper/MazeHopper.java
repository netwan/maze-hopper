package de.mazehopper;

import de.mazehopper.map.Map;
import de.mazehopper.map.RandomMap;

public class MazeHopper {
	private Display display;
	private Map map;
	
	public MazeHopper() {
		map = new RandomMap(101, 101);
		display = new Display(map);
		System.out.println(map.toString());
	}

	public static void main(String[] args) {
		new MazeHopper();
	}

}
