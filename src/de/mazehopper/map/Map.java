package de.mazehopper.map;

public abstract class Map {
	protected int width, height;
	protected FieldType[][] map;
	
	public Map(int width, int height) {
		this.width = width;
		this.height = height;
		this.map = new FieldType[width][height];
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (int x = 0; x < width; x++)
			sb.append('#');
		sb.append("##\n#");
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (map[x][y] == FieldType.FREE)
					sb.append(' ');
				else
					sb.append('#');
			}
			sb.append("#\n#");
		}
		for (int x = 0; x < width+1; x++)
			sb.append('#');
		return sb.toString();
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public FieldType getFieldAt(int x, int y) {
		if (x < 0 || x >= getWidth() || y < 0 || y >= getHeight())
			throw new IllegalArgumentException("The requested field must exist!");
		return map[x][y];
	}
}
