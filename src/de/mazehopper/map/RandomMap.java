package de.mazehopper.map;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

public class RandomMap extends Map {
	
	private Random rng;

	public RandomMap(int width, int height) {
		super(width, height);
		
		rng = new Random();
		
		generateMaze();
	}

	private void generateMaze() {
		ArrayList<Point> stack = new ArrayList<Point>();
		
		// initialize map
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
				map[x][y] = FieldType.WALL;
		
		int[] directions = {-1, 0,  1, 0,  0, -1,  0, 1};
		
		stack.add(new Point(0, 0));
		int x = 0, y = 0;
		while (stack.size() > 0) {
			map[x][y] = FieldType.FREE;
			int r = rng.nextInt(4);
			boolean stuck = true;
			for (int i = 0; i < 4; i++) {
				int dir = (r + i) % 4;
				int nx = x + directions[2*dir];
				int ny = y + directions[2*dir+1];
				int nnx = nx + directions[2*dir];
				int nny = ny + directions[2*dir+1];
				if (nnx >= 0 && nny >= 0 && nnx < width && nny < height
						&& map[nnx][nny] == FieldType.WALL) {
					map[nx][ny] = FieldType.FREE;
					x = nnx;
					y = nny;
					stack.add(new Point(nnx, nny));
					stuck = false;
					break;
				}
			}
			if (stuck) {
				Point lastPoint = stack.remove(stack.size()-1);
				x = lastPoint.x;
				y = lastPoint.y;
			}
		}
	}
}
